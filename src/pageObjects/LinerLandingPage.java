package pageObjects;

import org.openqa.selenium.By;

public class LinerLandingPage {

    //public String pageURL = "https://dev.stage.domrf.orbita.center/liner";
    public String pageURL = "https://dev.domrf.cometrica.ru/liner";

    public By landingLinerElement = By.xpath("//div[@class='front-page-line__offer-box-title ng-binding'][text()='Арендное жилье в 10 минутах от центра']");

    public By galleryHeader = By.xpath("//div[@class='front-page-line__offer-box-title ng-binding'][text()='Арендное жилье в 10 минутах от центра']");

    public By prevControlInGallery = By.xpath("//div[@class='tru-slider__handler tru-slider__handler--prev']");

    public By nextControlInGallery = By.xpath("//div[@class='tru-slider__handler tru-slider__handler--next']");

    public By closeModalPhotos = By.xpath("//div[@class='modal-photos__close']");

    public By leftControlPhotos = By.xpath("//div[@class='tru-slider__handler arr-left']");

    public By rightControlPhotos = By.xpath("//div[@class='tru-slider__handler arr-right']");

    public By findApart = By.xpath("//button[text()='Подобрать жильё']");

    public By lookOnMap = By.xpath("//div[@id='infrastructure']/button[@class='btn btn--green btn--l btn--lightSdO ng-binding'][text()='Посмотреть на карте']");

    public By popUpShop = By.xpath("//div[@class='front-page-line__environment-box-plan']/i[1]");

    public By popUpPark = By.xpath("//div[@class='front-page-line__environment-box-plan']/i[4]");

    public By popUpSport = By.xpath("//div[@class='front-page-line__environment-box-plan']/i[6]");

    public By popUpTransport = By.xpath("//div[@class='front-page-line__environment-box-plan']/i[7]");

    public By popUpParking = By.xpath("//div[@class='front-page-line__comfort-pic']/i[3]");

    public By popUpSafe = By.xpath("//div[@class='front-page-line__comfort-pic']/i[4]");

    public By popUpInto = By.xpath("//div[@class='front-page-popup__info']/div[4]");

    public By lookOnMapInPopUp = By.xpath("//div[@class='front-page-popup__info']/button");

    public By closePopUp = By.xpath("//div[@class='front-page-popup__close']");

    public By picNoCar = By.xpath("//div[@class='front-page-line__comfort-wrap']/div[1]");

    public By picParking = By.xpath("//div[@class='front-page-line__comfort-wrap']/div[2]");

    public By picUK = By.xpath("//div[@class='front-page-line__comfort-wrap']/div[3]");

    public By flatPlan = By.xpath("//div[@class='front-page-line__flat-box-wrap']/div[2]");

    public By sendApplicationInPlan = By.xpath("//div[@class='front-page-line__flat-box-content front-page-line-box-content']/button[text()='Оставить заявку']");

    public By apartsGallery = By.xpath("//div[@class='front-page-line front-page-line front-page-line__photo-text']//div[@class='front-page-line__photo-box-content front-page-line-box-content']");

    public By otherApartsGallery = By.xpath("//div[@class='front-page-line front-page-line__photo-list']/div[3]");

    public By leftControlApartsPhotos = By.xpath("//div[@class='front-page-line__photo-box-content front-page-line-box-content']/div[@class='tru-slider__handler arr-left']");

    public By rightControlApartsPhotos = By.xpath("//div[@class='front-page-line__photo-box-content front-page-line-box-content']/div[@class='tru-slider__handler arr-right']");

    public By sendApplicationAparts = By.xpath("//div[@class='front-page-line__photo-box-content front-page-line-box-content']/button[text()='Оставить заявку']");

    public By questAndAnsw = By.xpath("//div[@class='front-page-line__quest-box-wrap']//v-pane-header[@class='ng-binding ng-scope ng-isolate-scope'][contains(text(),'Какой минимальный срок аренды')]");

    public By textInQuestAndAnswArticle = By.xpath("//div[@class='front-page-line__quest-box-wrap']//v-accordion[1]//v-pane[3]//v-pane-content[1]//div[1]//div[1]");

    public By inputName = By.xpath("//input[@name='name']");

    public By inputPhone = By.xpath("//input[@name='phone']");

    public By sendApplicationFooter = By.xpath("//form[@name='orderForm']//button[contains(text(), 'Оставить заявку')]");

    public By thanksForApplication = By.xpath("//div[contains(text(), 'спасибо за заявку')]");

    public By newApplication = By.xpath("//div[contains(text(), 'новую заявку')]");

    public By logoSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']/a[1]");

    public By homePageElement = By.xpath("//button[contains(text(), 'Подобрать жильё')]");

    public By searchPageElement = By.xpath("//div[@class='aptm_search-find ng-binding'][contains(text(), 'Найдено')]");

    public By rentersPageElement = By.xpath("//div[@class='partners-title ng-binding']");

    public By partnersPageElement = By.xpath("//div[@class='partners-title ng-binding']");

    public By investorsPageElement = By.xpath("//div[@class='investors-title ng-binding']");

    public By massMediaPageElement = By.xpath("//div[@class='massmedia-title ng-binding']");

    public By authPageElement = By.xpath("//h1[@class='auth-page-caption ng-binding'][text()='Вход в личный кабинет жильца']");

    public By findApartSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']//a[@class='std-pg-header-link-item ng-binding ng-scope'][text()='Подобрать жильё']");

    public By rentersSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']//a[@class='std-pg-header-link-item ng-binding ng-scope'][text()='Арендаторам']");

    public By partnersSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']//a[@class='std-pg-header-link-item ng-binding ng-scope'][text()='Партнёрам']");

    public By investorsSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']//a[@class='std-pg-header-link-item ng-binding ng-scope'][text()='Инвесторам']");

    public By massMediaSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']//a[@class='std-pg-header-link-item ng-binding ng-scope'][text()='СМИ о нас']");

    public By cabinetSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//a[@class='std-pg-header__login-link std-pg-header__login-link-mobile']/div[text()='Кабинет жильца']");

}