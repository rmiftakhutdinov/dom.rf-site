package pageObjects;

import org.openqa.selenium.By;

public class InvestorsPage {

    //public String pageURL = "https://dev.stage.domrf.orbita.center/for-investors";
    public String pageURL = "https://dev.domrf.cometrica.ru/for-investors";

    public By investorsPageElement = By.xpath("//div[@class='investors-title ng-binding']");

    public By liner = By.xpath("//a[@ui-sref='liner']");

    public By landingLinerElement = By.xpath("//div[@class='front-page-line__offer-box-title ng-binding'][text()='Арендное жилье в 10 минутах от центра']");

    public By listOfCompany = By.xpath("//a[@class='partners-file_link'][text()='здесь']");

    public By elementListOfCompany = By.xpath("//h1[text()='О компании']");

}
