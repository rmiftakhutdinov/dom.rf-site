package pageObjects;

import org.openqa.selenium.By;

public class HomePage {

    public String pageURL = "https://dev.stage.domrf.orbita.center/";
    //public String pageURL = "https://dev.domrf.cometrica.ru/";

    public By homePageElement = By.xpath("//button[contains(text(), 'Подобрать жильё')]");

    public By findApart = By.xpath("//button[contains(text(), 'Подобрать жильё')]");

    public By liner = By.xpath("//div[@class='title ng-binding'][text()='Лайнер']");

    public By landingLinerElement = By.xpath("//div[@class='front-page-line__offer-box-title ng-binding'][text()='Арендное жилье в 10 минутах от центра']");

    public By nextPage = By.xpath("//div[@class='swiper-button-next tru-slider__handler']");

    public By prevPage = By.xpath("//div[@class='swiper-button-prev tru-slider__handler']");

    public By toSearchPageLink = By.xpath("//button[contains(text(),'Подобрать жильё')]");

    public By searchPageElement = By.xpath("//div[@class='aptm_search-find ng-binding'][contains(text(), 'Найдено')]");

    public By menuAllDirectionsAhml = By.xpath("//span[text()='Все направления ДОМ.РФ']");

    public By toSiteGroupLink = By.xpath("//a[contains(text(),'Перейти на сайт группы')]");

    public By siteGroupPageElement = By.xpath("//h1[text()='Новое качество жизни']");

    public By sendApplicationFooterInEnglish = By.xpath("//form[@name='orderForm']//button[contains(text(), 'Make request')]");

    public By logoCabinet = By.xpath("//a[@class='auth-page-logo logo']");

    public By findApartHeader = By.xpath("//a[text()='Подобрать жильё']");

    public By rentersHeader = By.xpath("//div[@class='std-pg-header-link']/a[@ui-sref='renters']");

    public By partnersHeader = By.xpath("//div[@class='std-pg-header-link']/a[@ui-sref='partners']");

    public By investorsHeader = By.xpath("//div[@class='std-pg-header-link']/a[@ui-sref='investors']");

    public By massMediaHeader = By.xpath("//div[@class='std-pg-header-link']/a[@ui-sref='massmedia']");

    public By cabinet = By.xpath("//div[@class='std-pg-header__login-link-text ng-binding'][text()='Кабинет жильца']");

    public By changeToRussian = By.xpath("//a[@class='std-pg-header__lang-item std-pg-header__lang-item--ru ng-scope']");

    public By changeToEnglish = By.xpath("//a[@class='std-pg-header__lang-item std-pg-header__lang-item--en ng-scope']");

    public By changeToRussianSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//a[@class='std-pg-header__lang-item std-pg-header__lang-item--ru ng-scope']");

    public By changeToEnglishSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//a[@class='std-pg-header__lang-item std-pg-header__lang-item--en ng-scope']");

    public By logoHeader = By.xpath("//a[@class='s-ico s-ico-logo std-pg-header-logo ng-scope']");

    public By doc1 = By.xpath("//div[@class='std-pg-footer-box-link']/a[1]");

    public By doc2 = By.xpath("//div[@class='std-pg-footer-box-link']/a[2]");

    public By doc3 = By.xpath("//div[@class='std-pg-footer-box-link']/a[3]");

    public By facebook = By.xpath("//img[@alt='facebook']");

    public By facebookElement = By.xpath("//i/u[text()='Facebook']");

    public By instagram = By.xpath("//img[@alt='instagram']");

    public By instagramElement = By.xpath("//h1[@class='rhpdm'][text()='Аренда для жизни']");

    public By vkontakte = By.xpath("//img[@alt='vkontakte']");

    public By vkontakteElement = By.xpath("//div[@class='top_home_logo']");

}
