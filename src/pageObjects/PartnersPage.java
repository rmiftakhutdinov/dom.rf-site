package pageObjects;

import org.openqa.selenium.By;

public class PartnersPage {

    //public String pageURL = "https://dev.stage.domrf.orbita.center/for-partners";
    public String pageURL = "https://dev.domrf.cometrica.ru/for-partners";

    public By partnersPageElement = By.xpath("//div[text()='Партнёрам']");

    public By document = By.xpath("//a[@href='https://dev-bk.stage.domrf.orbita.center/documents/poryadok_i_usloviya_finansirovaniya.pdf']");

}
