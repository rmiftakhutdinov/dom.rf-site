package pageObjects;

import org.openqa.selenium.By;

public class RentersPage {

    //public String pageURL = "https://dev.stage.domrf.orbita.center/for-renters";
    public String pageURL = "https://dev.domrf.cometrica.ru/for-renters";

    public By rentersPageElement = By.xpath("//div[text()='Арендаторам']");

    public By parking = By.xpath("//v-pane-header[@class='ng-binding ng-scope ng-isolate-scope'][text()='Предусмотрена ли парковка']");

    public By parkingElement = By.xpath("//div[@class='ng-binding ng-scope'][contains(text(),'Для жителей Арендного дома «Лайнер»')]");

    public By internet = By.xpath("//v-pane-header[@class='ng-binding ng-scope ng-isolate-scope'][text()='Есть ли Интернет в апартаментах']");

    public By internetElement = By.xpath("//*[text()='Есть ли Интернет в апартаментах']/following-sibling::*/div/div");

    public By file1 = By.xpath("//a[@href='https://dev-bk.stage.domrf.orbita.center/documents/Lajner_tipovoi_dogovor_bronirovanija_s_fiz_licom.pdf']");

    public By file2 = By.xpath("//a[@href='https://dev-bk.stage.domrf.orbita.center/documents/Lajner_soglasie_na_obrabotky_pers_dannih.pdf']");

    public By file3 = By.xpath("//a[@href='https://dev-bk.stage.domrf.orbita.center/documents/Lajner_dogovor_arendy.pdf']");

    public By file4 = By.xpath("//a[@href='https://dev-bk.stage.domrf.orbita.center/documents/Dolgosrochnyj_dogovor_arendy_s_FL_redakcija_1.pdf']");

    public By file5 = By.xpath("//a[@href='https://dev-bk.stage.domrf.orbita.center/documents/Lajner_dogovor_arendy_avto_fiz_lico.pdf']");

    public By file6 = By.xpath("//a[@href='https://dev-bk.stage.domrf.orbita.center/documents/Prejskurant_na_dopolnitelnye_uslugi_10_05.pdf']");

    public By file7 = By.xpath("//a[@href='https://dev-bk.stage.domrf.orbita.center/documents/living_rules.pdf']");

}
