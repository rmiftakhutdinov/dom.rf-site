package pageObjects;

import org.openqa.selenium.By;

public class SearchPage {

    //public String pageURLTest = "http://t1-ahml.22slona.ru/search";

    //public String pageURLDev = "https://dev.stage.domrf.orbita.center/search";
    public String pageURLDev = "https://dev.domrf.cometrica.ru/search";

    public By landingLinerElement = By.xpath("//div[@class='front-page-line__offer-box-title ng-binding'][text()='Арендное жилье в 10 минутах от центра']");

    public By searchPageElement = By.xpath("//div[@class='aptm_search-find ng-binding'][contains(text(), 'Найдено')]");

    public By roomStudio = By.xpath("//label[text()='Студия']");

    public By room2 = By.xpath("//label[text()='2']");

    public By room3 = By.xpath("//label[text()='3']");

    //public By notEndFloor = By.xpath("//input[@id='check']");
    public By notEndFloor = By.xpath("//label[@for='check']");

    public By floorFrom = By.xpath("//input[@ng-model='filter.floor.from']");

    public By floorTo = By.xpath("//input[@ng-model='filter.floor.to']");

    public By floor15 = By.xpath("//div[@class='aptm_search-item_title ng-binding']");

    public By priceLow = By.xpath("//div[@class='handle low']");

    public By priceHigh = By.xpath("//div[@class='handle high']");

    public By sliderTrack = By.cssSelector("div.ng-isolate-scope.angular-range-slider");

    public By showFilter = By.xpath("//span[@class='aptm_search-filter_link ng-binding']");

    public By showApartments = By.xpath("//a[@class='aptm_search-map-balloon_link']");

    public By page1 = By.xpath("//span[@class='page ng-binding ng-scope'][text()='1']");

    public By page1Active = By.xpath("//div[@class='pagination ng-scope']/span[1][@class='page ng-binding ng-scope active']");

    public By page2 = By.xpath("//span[@class='page ng-binding ng-scope'][text()='2']");

    public By page2Active = By.xpath("//div[@class='pagination ng-scope']/span[2][@class='page ng-binding ng-scope active']");

    public By pageNext = By.xpath("//span[@ng-click='goToPage(filter.page+1)'][contains(text(), 'Дальше')]");

    public By apartment = By.xpath("//div[@ng-if='!listLoading && aptmList.length']/a[1]");

    public By resetFilters = By.xpath("//span[text()='сбросьте фильтры']");

    public By apartmentPageElement = By.xpath("//div[@class='details-block']/div[contains(text(), 'Подробнее')]");

    public By noApartmentsElement = By.xpath("//h4[text()='Свободных апартаментов нет']");

    public By inputName = By.xpath("//input[@name='name']");

    public By inputPhone = By.xpath("//input[@name='phone']");

    public By sendOrder = By.xpath("//button[text()='Отправить']");

    public By sendOrderElement = By.xpath("//div[contains(text(), 'спасибо за ваш интерес')]");

    public By spinner = By.xpath("//div[@class='loader']");

}
