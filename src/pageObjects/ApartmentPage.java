package pageObjects;

import org.openqa.selenium.By;

public class ApartmentPage {

    public String pageURL = "https://dev.stage.domrf.orbita.center/search";
    //public String pageURL = "https://dev.domrf.cometrica.ru/search";

    public By apartmentPageElement = By.xpath("//div[@class='details-block']/div[contains(text(), 'Подробнее')]");

    public By liner = By.xpath("//a[@class='landing-link ng-binding'][contains(text(), 'Лайнер')]");

    public By landingLinerElement = By.xpath("//div[@class='front-page-line__offer-box-title ng-binding'][text()='Арендное жилье в 10 минутах от центра']");

    public By gallery = By.xpath("//div[@class='tru-slider ng-scope']/div[3]/div[2]/div[2]");

    public By nextControl = By.xpath("//div[@class='tru-slider__handler tru-slider__handler--next']");

    public By prevControl = By.xpath("//div[@class='tru-slider__handler tru-slider__handler--prev']");

    public By closeModalPhotos = By.xpath("//div[@class='modal-photos__close']");

    public By miniPhotos = By.xpath("//div[@class='tru-slider__thumbs ng-scope']/div[3]/img[@class='tru-slider__thumb-image']");

    public By reserve = By.xpath("//div[@class='aptm-pg-top__view-order-button btn btn--green btn--l btn--lightSdO fx-row fx-center ng-binding']");

    public By inputName = By.xpath("//input[@name='clientFirstName']");

    public By inputEmail = By.xpath("//input[@name='clientEmail']");

    public By inputPhone = By.xpath("//input[@name='clientPhone']");

    public By inputSnils = By.xpath("//input[@name='clientCode']");

    public By inputPasportSeriesNumber = By.xpath("//input[@name='passportSeriesNumber']");

    public By textareaWhoGiven = By.xpath("//textarea[@name='passportIssued']");

    public By inputDateGiven = By.xpath("//input[@name='passportIssuedAt']");

    public By inputCodeDepartament = By.xpath("//input[@name='passportCode']");

    public By nextStep = By.xpath("//button[contains(text(), 'Далее')]");

    public By reservePopUp = By.xpath("//button[contains(text(), 'Забронировать')]");

    public By popUpElement = By.xpath("//div[text()='Спасибо за заявку!']");

    public By newReserve = By.xpath("//div[@ng-click='clearReserv()']");

    public By closePopUp = By.xpath("//div[@class='front-page-popup__close reserv-popup__close']");

    public By more = By.xpath("//a[text()='Подробнее']");

    public By passportPhoto = By.xpath("//label[@class='reserv-popup__file-label s-ico s-ico-clip ng-binding']");

    public String userNameApart = "Тест";
    public String userEmailApart = "test@test.ru";
    public String userPhoneApart = "55553332211";
    public String snils = "55544433322";
    public String seriesNumber = "9200 776655";
    public String whoGiven = "Администрация Тринадцатого района Парижской области";
    public String dateGiven = "12122000";
    public String codeDepartament = "555-777";

    public By apartment = By.xpath("//div[@ng-if='!listLoading && aptmList.length']/a[1]");

    public By searchPageElement = By.xpath("//div[@class='aptm_search-find ng-binding'][contains(text(), 'Найдено')]");

}
