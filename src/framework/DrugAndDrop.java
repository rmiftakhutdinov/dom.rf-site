package framework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class DrugAndDrop extends Helper {

    private Integer getStep(WebElement SliderTrack) {

        return SliderTrack.getSize().width / 4;

    }

    private Integer getCurrentPosition(WebElement priceLow, WebElement sliderTrack) {

        Integer sliderCenterPx = Integer.parseInt(priceLow.getCssValue("left").replaceAll("px", "")) + priceLow.getSize().width / 2;
        return sliderCenterPx / getStep(sliderTrack) + 1;

    }

    protected void setSliderPosition(Integer position, WebElement sliderTrack, WebElement priceLow) {

        Integer xOffset = (position - getCurrentPosition(priceLow, sliderTrack)) * getStep(sliderTrack);
        Actions actions = new Actions(driver);
        actions.dragAndDropBy(priceLow, xOffset, 0).perform();

    }

}
