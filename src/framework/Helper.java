package framework;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.util.Objects;

public class Helper {

    // Вызов драйвера
    protected WebDriver driver;

    // Выбор драйвера и браузера
    protected WebDriver getDriver(String os){

        if (Objects.equals(os.toLowerCase(), "windows")){

            System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        } else {

            System.setProperty("webdriver.chrome.driver", "drivers/chrome/chromedriver");

        }

        return new ChromeDriver();

    }

    protected FirefoxDriver getFirefoxDriver (String os) {

        if (Objects.equals(os.toLowerCase(), "windows")){

            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");

        } else {

            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver");

        }

        // Запуск тестов без открытия браузера
        /*ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        return new ChromeDriver(options);*/

        return new FirefoxDriver();

    }

    // Сообщение об ошибке
    protected void afterExeptionMessage(String message){

        throw new RuntimeException("\n" + message);

    }

    // Таймаут по заданному времени
    protected void sleep(int time) throws InterruptedException {

        Thread.sleep(time);

    }

    // Клик на элемент
    protected void click(By by){

        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(by)).click();

    }

    // Ожидание элемента и клик по нему
    protected void click(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeClickable(by)).click();

    }

    // Ожидание элемента с выбором времени
    protected void waitBy(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.visibilityOfElementLocated(by));

    }

    // Ожидание элемента с установленным по дефолту временем
    protected void waitBy(By by){

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(by));

    }

    // Поиск и ожидание элемента
    protected WebElement findElement(By by){

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (java.util.NoSuchElementException ex){
            throw new RuntimeException("Элемент " + by + " не найден на странице");
        }

        return driver.findElement(by);

    }

    // Скролл вниз (хз как работает)
    protected void scrollDown() {

        ((JavascriptExecutor)driver).executeScript("window.scroll(0, window.innerHeight);");

    }

    // Скролл к элементу
    protected void scrollToElement(By by) throws InterruptedException {

        WebElement element = driver.findElement(by);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        sleep(1000);

    }

    // Поиск элемента на странице
    protected boolean isElementPresent(By selector){

        try{
            driver.findElement(selector);
            return true;
        } catch (NoSuchElementException ex){
            return false;
        }

    }

    // Добавление файла
    protected void setClipboardData(String string) {

        StringSelection stringSelection = new StringSelection(string);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

    }

    // Сендкейс - вставить текст
    protected void sendKeys(By by, String string){

        findElement(by).clear();
        findElement(by).sendKeys(string);

    }

    // Сендкейс - нажать с клавиатуры
    protected void sendKeys(By by, Keys keys){

        findElement(by).clear();
        findElement(by).sendKeys(keys);

    }

    // Ожидание исчезновения элемента
    protected void waitForElementIsNotPresent(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.invisibilityOf(driver.findElement(by)));

    }

    // Ожидание исчезновения элемента
    protected void waitForElementIsNotPresent(By by){

        new WebDriverWait(driver, 5).until(ExpectedConditions.invisibilityOf(driver.findElement(by)));

    }

    // Ожидание отсутствия элемента на странице
    protected void isElementNotPresent(By selector){

        try{

            int timeouts = 3;
            new WebDriverWait(driver, timeouts).until(ExpectedConditions.presenceOfElementLocated(selector));

        } catch (TimeoutException ignored){

        }

    }

    /*protected boolean isElementNotPresent(By selector){

        try{

            int timeouts = 3;
            new WebDriverWait(driver, timeouts).until(ExpectedConditions.presenceOfElementLocated(selector));
            return false;

        } catch (TimeoutException ex){

            return true;

        }

    }*/

}
