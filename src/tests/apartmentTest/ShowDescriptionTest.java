package tests.apartmentTest;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.ApartmentPage;
import java.util.concurrent.TimeUnit;

public class ShowDescriptionTest extends Helper {

    @BeforeMethod
    public void setUp() {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(10000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(10000, TimeUnit.MILLISECONDS);

        ApartmentPage page = new ApartmentPage();
        driver.get(page.pageURL);
        try {
            waitBy(page.searchPageElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница поиска не загрузилась");
        }

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Раскрыть текст описания апартамента по кнопке 'Подробнее'")
    public void showDescriptionOfApartment() {

        ApartmentPage page = new ApartmentPage();

        click(page.apartment);
        try {
            driver.findElement(page.apartmentPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница апартамента не загрузилась");
        }

        click(page.more);
        isElementNotPresent(page.more);

    }

}
