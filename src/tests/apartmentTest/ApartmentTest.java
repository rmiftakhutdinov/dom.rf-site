package tests.apartmentTest;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.ApartmentPage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class ApartmentTest extends Helper {

    @BeforeMethod
    public void setUp() {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        ApartmentPage page = new ApartmentPage();
        driver.get(page.pageURL);
        try {
            waitBy(page.searchPageElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница поиска не загрузилась");
        }

        click(page.apartment);
        try {
            driver.findElement(page.apartmentPageElement).isDisplayed();
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница апартамента не загрузилась");
        }

        waitBy(page.gallery,30);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход по ссылке на лендинг Лайнер")
    public void clickLiner() {

        ApartmentPage page = new ApartmentPage();

        click(page.liner);
        try {
            driver.findElement(page.landingLinerElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница лендинга не загрузилась");
        }

    }

    @Test(description = "Прокликать фотогалерею", priority = 1)
    public void clickGallery() throws InterruptedException {

        ApartmentPage page = new ApartmentPage();

        click(page.nextControl);
        sleep(1000);
        click(page.prevControl);
        sleep(1000);

        click(page.miniPhotos);
        sleep(1000);
        click(page.gallery);
        sleep(1000);
        click(page.closeModalPhotos);
        sleep(1000);

        click(page.gallery);
        sleep(1000);
        click(page.nextControl);
        sleep(1000);
        click(page.prevControl);
        sleep(1000);
        click(page.closeModalPhotos);

    }

    @Test(description = "Забронировать апартамент", priority = 2)
    public void reserveApart() throws InterruptedException, AWTException {

        ApartmentPage page = new ApartmentPage();

        sleep(500);

        click(page.reserve);

        driver.findElement(page.inputName).clear();
        driver.findElement(page.inputName).sendKeys(page.userNameApart);
        driver.findElement(page.inputEmail).clear();
        driver.findElement(page.inputEmail).sendKeys(page.userEmailApart);
        driver.findElement(page.inputPhone).clear();
        driver.findElement(page.inputPhone).sendKeys(page.userPhoneApart);

        click(page.nextStep);
        waitBy(page.inputSnils, 10);

        driver.findElement(page.inputSnils).clear();
        driver.findElement(page.inputSnils).sendKeys(page.snils);
        driver.findElement(page.inputPasportSeriesNumber).clear();
        driver.findElement(page.inputPasportSeriesNumber).sendKeys(page.seriesNumber);
        driver.findElement(page.textareaWhoGiven).clear();
        driver.findElement(page.textareaWhoGiven).sendKeys(page.whoGiven);
        driver.findElement(page.inputDateGiven).clear();
        driver.findElement(page.inputDateGiven).sendKeys(page.dateGiven);
        driver.findElement(page.inputCodeDepartament).clear();
        driver.findElement(page.inputCodeDepartament).sendKeys(page.codeDepartament);

        // JPG
        String jpg = System.getProperty("user.dir") + "\\files\\111.jpg";
        setClipboardData(jpg);
        click(page.passportPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        sleep(2000);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(5000);

        Assert.assertTrue(findElement(By.xpath("//div[1][@ng-repeat='file in loadedFiles']/img")).getAttribute("src").contains(".jpeg"));

        // PDF
        String pdf = System.getProperty("user.dir") + "\\files\\222.pdf";
        setClipboardData(pdf);
        click(page.passportPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        sleep(2000);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(5000);

        Assert.assertTrue(findElement(By.xpath("//div[@ng-repeat='file in loadedFiles']/a")).getAttribute("alt").equals("pdf"));

        // PNG
        String png = System.getProperty("user.dir") + "\\files\\333.png";
        setClipboardData(png);
        click(page.passportPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        sleep(2000);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(5000);

        Assert.assertTrue(findElement(By.xpath("//div[3][@ng-repeat='file in loadedFiles']/img")).getAttribute("src").contains(".png"));

        click(page.reservePopUp);
        driver.findElement(page.popUpElement);
        click(page.newReserve);
        waitBy(page.inputName, 10);
        driver.findElement(page.inputName);

        click(page.closePopUp);

    }

}



