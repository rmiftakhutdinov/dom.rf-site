package tests.linerLandingTest;

import framework.Configuration;
import org.openqa.selenium.*;
import pageObjects.HomePage;
import pageObjects.LinerLandingPage;
import data.CommonData;
import framework.Helper;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.assertTrue;

public class LinerLandingTest extends Helper {

    @BeforeMethod
    public void setUp() {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        LinerLandingPage page = new LinerLandingPage();
        driver.get(page.pageURL);
        try {
            waitBy(page.landingLinerElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент страницы лендинга 'Лайнер' не найден");
        }


    }

    @AfterMethod
    public void tearDown() {

        driver.quit();

    }

    @Test(description = "Переход по ссылкам в плавающем меню страницы")
    public void clickHeaderLinksSwimMenu() throws InterruptedException {

        LinerLandingPage page = new LinerLandingPage();
        HomePage homepage = new HomePage();

        scrollToElement(page.sendApplicationFooter);

        click(page.logoSwim);
        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Главная страница не загрузилась");
        }
        sleep(3000);
        driver.navigate().back();
        waitBy(page.landingLinerElement);

        scrollToElement(page.sendApplicationFooter);

        waitBy(page.findApartSwim);

        click(page.findApartSwim);
        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница поиска не загрузилась");
        }
        driver.navigate().back();
        waitBy(page.landingLinerElement);

        scrollToElement(page.sendApplicationFooter);

        waitBy(page.rentersSwim);

        click(page.rentersSwim);
        try {
            driver.findElement(page.rentersPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница 'Арендаторам' не загрузилась");
        }
        driver.navigate().back();
        waitBy(page.landingLinerElement);

        scrollToElement(page.sendApplicationFooter);

        waitBy(page.partnersSwim);

        click(page.partnersSwim);
        try {
            driver.findElement(page.partnersPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница 'Партнерам' не загрузилась");
        }
        driver.navigate().back();
        waitBy(page.landingLinerElement);

        scrollToElement(page.sendApplicationFooter);

        waitBy(page.investorsSwim);

        click(page.investorsSwim);
        try {
            driver.findElement(page.investorsPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница 'Инвесторам' не загрузилась");
        }
        driver.navigate().back();
        waitBy(page.landingLinerElement);

        scrollToElement(page.sendApplicationFooter);

        waitBy(page.massMediaSwim);

        click(page.massMediaSwim);
        try {
            driver.findElement(page.massMediaPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница 'СМИ о нас' не загрузилась");
        }
        driver.navigate().back();
        waitBy(page.landingLinerElement);

        scrollToElement(page.sendApplicationFooter);

        click(page.cabinetSwim);
        try {
            driver.findElement(page.authPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница авторизации не загрузилась");
        }

        click(homepage.logoCabinet);
        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Главная страница не загрузилась");
        }

        click(homepage.liner);
        try {
            driver.findElement(page.landingLinerElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница лендинга не загрузилась");
        }

        scrollToElement(new LinerLandingPage().sendApplicationFooter);

        driver.findElement(homepage.changeToEnglishSwim);
        click(homepage.changeToEnglishSwim);
        try {
            driver.findElement(homepage.changeToRussian);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Перевод на английский язык не сработал");
        }

        sleep(1000);
        scrollToElement(homepage.sendApplicationFooterInEnglish);

        driver.findElement(homepage.changeToRussianSwim);
        click(homepage.changeToRussianSwim);
        try {
            driver.findElement(homepage.changeToEnglish);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Перевод на русский язык не сработал");
        }

    }

    @Test(description = "Открыть и закрыть окно карты", priority = 1)
    public void openMap() throws InterruptedException {

        LinerLandingPage page = new LinerLandingPage();

        String handleBefore = driver.getWindowHandle();

        click(page.lookOnMap);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String id = "oid=1406479145";
        Assert.assertTrue(driver.getCurrentUrl().contains(id));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.popUpPark);

        String handleBefore1 = driver.getWindowHandle();

        click(page.lookOnMapInPopUp);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String id1 = "oid=1406479145";
        Assert.assertTrue(driver.getCurrentUrl().contains(id1));
        driver.close();
        driver.switchTo().window(handleBefore1);
        click(page.closePopUp);

        click(page.popUpShop);

        String handleBefore2 = driver.getWindowHandle();

        click(page.lookOnMapInPopUp);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String id2 = "oid=1406479145";
        Assert.assertTrue(driver.getCurrentUrl().contains(id2));
        driver.close();
        driver.switchTo().window(handleBefore2);
        click(page.closePopUp);

        click(page.popUpTransport);

        String handleBefore3 = driver.getWindowHandle();

        click(page.lookOnMapInPopUp);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String id3 = "oid=1406479145";
        Assert.assertTrue(driver.getCurrentUrl().contains(id3));
        driver.close();
        driver.switchTo().window(handleBefore3);
        click(page.closePopUp);

    }

    @Test(description = "Открыть поп-ап окна и прокликать их", priority = 2)
    public void clickPopUp() throws InterruptedException {

        LinerLandingPage page = new LinerLandingPage();

        click(page.popUpPark);
        click(page.popUpInto);
        click(page.popUpInto);
        click(page.closePopUp);

        sleep(500);

        click(page.popUpShop);
        click(page.popUpInto);
        click(page.popUpInto);
        click(page.closePopUp);

        sleep(500);

        click(page.popUpSport);
        click(page.popUpInto);
        click(page.popUpInto);
        click(page.closePopUp);

        sleep(500);

        click(page.popUpSport);
        click(page.closePopUp);

        sleep(500);

        click(page.popUpTransport);
        click(page.popUpInto);
        click(page.popUpInto);
        click(page.closePopUp);

        sleep(500);

        click(page.popUpTransport);
        click(page.closePopUp);

        click(page.popUpSafe);
        click(page.popUpInto);
        click(page.popUpInto);
        click(page.closePopUp);

        sleep(500);

        click(page.popUpSafe);
        click(page.closePopUp);

        click(page.popUpParking);
        click(page.popUpInto);
        click(page.closePopUp);

        sleep(500);

        click(page.picNoCar);
        click(page.popUpInto);
        click(page.closePopUp);

        click(page.picParking);
        click(page.popUpInto);
        click(page.closePopUp);

        sleep(500);

        click(page.picUK);
        click(page.closePopUp);

    }

    @Test(description = "Прокликать фотогалерею", priority = 3)
    public void clickBodyGalery() throws InterruptedException {

        LinerLandingPage page = new LinerLandingPage();

        click(page.galleryHeader);
        sleep(500);
        click(page.prevControlInGallery);
        sleep(1000);
        click(page.nextControlInGallery);
        sleep(1000);
        click(page.closeModalPhotos);

        click(page.leftControlPhotos);
        click(page.closeModalPhotos);

        click(page.rightControlPhotos);
        click(page.closeModalPhotos);

        click(page.findApart);
        sleep(500);
        driver.navigate().back();
        waitBy(page.landingLinerElement);

        click(page.apartsGallery);
        sleep(500);
        click(page.prevControlInGallery);
        sleep(1000);
        click(page.nextControlInGallery);
        sleep(1000);
        click(page.closeModalPhotos);

        click(page.leftControlApartsPhotos);
        click(page.closeModalPhotos);

        sleep(500);

        click(page.rightControlApartsPhotos);
        click(page.closeModalPhotos);

        click(page.otherApartsGallery);
        sleep(500);
        click(page.closeModalPhotos);

        click(page.sendApplicationAparts);
        sleep(500);

        click(page.flatPlan);
        sleep(500);
        click(page.prevControlInGallery);
        sleep(1000);
        click(page.nextControlInGallery);
        sleep(1000);
        click(page.closeModalPhotos);

        click(page.sendApplicationInPlan);
        sleep(500);

    }

    @Test(description = "Свернуть и развернуть блок 'Вопросы и ответы'", priority = 4)
    public void hideAndShowQuestionsAndAnswers() throws InterruptedException {

        LinerLandingPage page = new LinerLandingPage();

        click(page.questAndAnsw);
        sleep(500);
        Assert.assertTrue(driver.findElement(page.textInQuestAndAnswArticle).getText().contains("Договор аренды заключается"));
        sleep(1000);
        click(page.questAndAnsw);
        sleep(500);
        Assert.assertFalse(driver.findElement(page.textInQuestAndAnswArticle).isDisplayed());

    }

    @Test(description = "Отправить заявку", priority = 5)
    public void sendApplication() {

        LinerLandingPage page = new LinerLandingPage();
        CommonData user = new CommonData();

        driver.findElement(page.inputName).sendKeys(user.userName);
        driver.findElement(page.inputPhone).sendKeys(user.userPhone);

        List<WebElement> labels = driver.findElements(By.xpath("//label/span"));
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        click(page.sendApplicationFooter);
        waitBy(page.thanksForApplication);
        driver.findElement(page.thanksForApplication);
        click(page.newApplication);
        waitBy(page.inputName);
        driver.findElement(page.inputName);

    }

}

