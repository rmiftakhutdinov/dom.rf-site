package tests.investorsTest;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import pageObjects.InvestorsPage;

public class InvestorsPageTest extends Helper {

    @BeforeMethod
    public void setUp() {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        InvestorsPage page = new InvestorsPage();
        driver.get(page.pageURL);
        try {
            waitBy(page.investorsPageElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Элемент раздела 'Инвесторам' не найден");
        }

    }

    @AfterMethod
    public void tearDown() {

        driver.quit();

    }

    @Test(description = "Переход по ссылке на лендинг Лайнер")
    public void clickLiner() {

        InvestorsPage page = new InvestorsPage();

        click(page.liner);
        try {
            driver.findElement(page.landingLinerElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Страница лендинга не загрузилась");
        }

    }

    @Test(description = "Переход по ссылке на Перечеь компаний", priority = 1)
    public void clickOnCompanyList() throws InterruptedException {

        InvestorsPage page = new InvestorsPage();
        String handleBefore = driver.getWindowHandle();

        click(page.listOfCompany);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String id = "about/controls/";
        Assert.assertTrue(driver.getCurrentUrl().contains(id));

        try {
            driver.findElement(page.elementListOfCompany);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Страница 'О компании' не загрузилась");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

    }

}
