package tests.searchTest;

import framework.Configuration;
import framework.DrugAndDrop;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.SearchPage;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class SearchPageInDevServerTest extends DrugAndDrop {

    @BeforeMethod
    public void setUp(){

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        SearchPage page = new SearchPage();
        driver.get(page.pageURLDev);

        if (findElement(page.sendOrder).isDisplayed()){

            driver.quit();
            RuntimeException error = new SkipException("!!! СВОБОДНЫХ АПАРТАМЕНТОВ НЕТ !!!");
            error.printStackTrace();
            throw error;

        }

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Фильтрация по количеству комнат")
    public void filterRooms() {

        SearchPage page = new SearchPage();

        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы поиска не найден");
        }

        click(page.roomStudio);
        waitBy(page.spinner);
        waitForElementIsNotPresent(page.spinner);

        findElement(page.roomStudio).getAttribute("class").contains("active");

        click(page.roomStudio);
        waitBy(page.spinner);
        waitForElementIsNotPresent(page.spinner);

        click(page.room2);
        waitBy(page.spinner);
        waitForElementIsNotPresent(page.spinner);

        findElement(page.room2).getAttribute("class").contains("active");

        click(page.room2);
        waitBy(page.spinner);
        waitForElementIsNotPresent(page.spinner);

        click(page.room3);
        waitBy(page.spinner);
        waitForElementIsNotPresent(page.spinner);

        findElement(page.room3).getAttribute("class").contains("active");

        click(page.room3);
        waitBy(page.spinner);
        waitForElementIsNotPresent(page.spinner);

        click(page.roomStudio);
        waitBy(page.spinner);
        waitForElementIsNotPresent(page.spinner);

        if (isElementPresent(page.searchPageElement)){

            Set<String> elements = driver.findElements(By.xpath("//div[@class='aptm_search-item_header']/div[contains(text(),'Студия')]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());

            for (String element : elements) {

                String roomStudio[] = element.split(",");
                Assert.assertTrue(roomStudio[0].contains("Студия"));
                Assert.assertFalse(roomStudio[0].contains("2 ком."));
                Assert.assertFalse(roomStudio[0].contains("3 ком."));

            }

            click(page.roomStudio);
            waitBy(page.spinner);
            waitForElementIsNotPresent(page.spinner);

        } else {

            click(page.resetFilters);
            waitBy(page.spinner);
            waitForElementIsNotPresent(page.spinner);

        }

        click(page.room2);
        waitBy(page.spinner);
        waitForElementIsNotPresent(page.spinner);

        if (isElementPresent(page.searchPageElement)){

            Set<String> elements = driver.findElements(By.xpath("//div[@class='aptm_search-item_header']/div[contains(text(),'2 ком.')]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());

            for (String element : elements) {

                String roomStudio[] = element.split(",");
                Assert.assertFalse(roomStudio[0].contains("Студия"));
                Assert.assertTrue(roomStudio[0].contains("2 ком."));
                Assert.assertFalse(roomStudio[0].contains("3 ком."));

            }

            click(page.room2);
            waitBy(page.spinner);
            waitForElementIsNotPresent(page.spinner);

        } else {

            click(page.resetFilters);
            waitBy(page.spinner);
            waitForElementIsNotPresent(page.spinner);

        }

        click(page.room3);
        waitBy(page.spinner);
        waitForElementIsNotPresent(page.spinner);

        if (isElementPresent(page.searchPageElement)){

            Set<String> elements = driver.findElements(By.xpath("//div[@class='aptm_search-item_header']/div[contains(text(),'3 ком.')]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());

            for (String element : elements) {

                String roomStudio[] = element.split(",");
                Assert.assertFalse(roomStudio[0].contains("Студия"));
                Assert.assertFalse(roomStudio[0].contains("2 ком."));
                Assert.assertTrue(roomStudio[0].contains("3 ком."));

            }

            click(page.room3);
            waitBy(page.spinner);
            waitForElementIsNotPresent(page.spinner);

        } else {

            click(page.resetFilters);
            waitBy(page.spinner);
            waitForElementIsNotPresent(page.spinner);

        }

    }

    @Test(description = "Фильтрация по количеству этажей", priority = 1)
    public void filterFloors() {

        SearchPage page = new SearchPage();

        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы поиска не найден");
        }

        sendKeys(page.floorFrom, "12");
        sendKeys(page.floorTo, "15");
        Assert.assertFalse(driver.findElement(page.floor15).getText().contains("эт. 10/15"));

        sendKeys(page.floorFrom, "15");
        click(page.notEndFloor);
        try {
            driver.findElement(page.resetFilters).isDisplayed();
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки сброса фильтров не найден");
        }

    }

    @Test(description = "Фильтрация по цене", priority = 2)
    public void filterPrice() throws InterruptedException {

        SearchPage page = new SearchPage();

        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы поиска не найден");
        }

        WebElement priceLowStart = driver.findElement(page.priceLow);
        priceLowStart.getAttribute("style");
        WebElement priceHigh = driver.findElement(page.priceHigh);
        priceHigh.getAttribute("style");
        WebElement sliderTrack = driver.findElement(page.sliderTrack);

        setSliderPosition(2, sliderTrack, priceLowStart);
        WebElement priceLowFinish = driver.findElement(page.priceLow);
        priceLowFinish.getAttribute("style");
        Assert.assertFalse(priceLowStart == priceLowFinish);

        setSliderPosition(3, sliderTrack, priceHigh);
        WebElement priceHighFinish = driver.findElement(page.priceHigh);
        priceHighFinish.getAttribute("style");
        Assert.assertFalse(priceLowStart == priceLowFinish);

    }

    @Test(description = "Сброс фильтров", priority = 3)
    public void resetFilters() throws InterruptedException {

        SearchPage page = new SearchPage();

        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы поиска не найден");
        }

        driver.findElement(page.floorFrom).sendKeys("55");
        driver.findElement(page.floorTo).sendKeys("99");

        try {
            waitBy(page.resetFilters, 5);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент кнопки сброса фильтров не найден");
            // Тест падает из-за имеющегося бага! Вместо кнопки сброса фильтров выводится страница, информирующая об отсутствии свободных апартаментов!
        }

        sleep(1000);
        click(page.resetFilters);
        sleep(500);
        Assert.assertTrue(driver.findElement(page.searchPageElement).isDisplayed());

    }

    @Test(description = "Свернуть и развернуть фильтр", priority = 4)
    public void hideAndShowFilter() throws InterruptedException {

        SearchPage page = new SearchPage();

        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы поиска не найден");
        }

        sleep(1000);

        driver.findElement(page.apartment).sendKeys(Keys.PAGE_DOWN);
        waitBy(page.showFilter, 10);
        click(page.showFilter);
        driver.findElement(page.notEndFloor);

    }

    @Test(description = "Переход на карточку апартамента", priority = 5)
    public void clickApartment() {

        SearchPage page = new SearchPage();

        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы поиска не найден");
        }

        click(page.apartment);
        try {
            driver.findElement(page.apartmentPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы апартамента не найден");
        }

    }

    @Test(description = "Клик по кнопке 'Показать' в поп-ап окне на карте", priority = 6)
    public void clickShowApartments() {

        SearchPage page = new SearchPage();

        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы поиска не найден");
        }

        click(page.showApartments);
        try {
            waitBy(page.landingLinerElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент страницы ЖК 'Лайнер' не найден");
        }

    }

    @Test(description = "Пагинация", priority = 7)
    public void pagination() {

        SearchPage page = new SearchPage();

        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы поиска не найден");
        }

        if (isElementPresent(page.page2)){
            click(page.page2);
            Assert.assertTrue(isElementPresent(page.page2Active));
        } else {
            System.out.println("страницы 2 нет");
        }

        if (isElementPresent(page.page1)){
            click(page.page1);
            Assert.assertTrue(isElementPresent(page.page1Active));
        }

        if (isElementPresent(page.pageNext)){
            click(page.pageNext);
            Assert.assertTrue(isElementPresent(page.page2Active));
        }

    }

}
