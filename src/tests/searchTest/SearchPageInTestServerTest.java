package tests.searchTest;

import framework.Configuration;
import framework.DrugAndDrop;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.SearchPage;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.assertTrue;

public class SearchPageInTestServerTest extends DrugAndDrop {

    @BeforeMethod
    public void setUp(){

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        SearchPage page = new SearchPage();
        //driver.get(page.pageURLTest);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Отправка заявки при отсутствии свободных апартаментов")
    public void sendOrderFromSearchPage() {

        SearchPage page = new SearchPage();

        try {
            driver.findElement(page.noApartmentsElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы без свободных апартаментов не найден либо имеются свободные апартаменты");
        }

        driver.findElement(page.inputName).sendKeys("Клиент");
        driver.findElement(page.inputPhone).sendKeys("6667778899");

        List<WebElement> labels = driver.findElements(By.xpath("//label/span"));
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        click(page.sendOrder);
        try {
            driver.findElement(page.sendOrderElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на экран успешной отправки заявки не произошел");
        }

    }

}
