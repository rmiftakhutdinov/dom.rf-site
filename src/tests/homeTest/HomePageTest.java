package tests.homeTest;

import framework.Configuration;
import framework.Helper;
import org.testng.Assert;
import pageObjects.HomePage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.LinerLandingPage;
import java.util.concurrent.TimeUnit;

public class HomePageTest extends Helper {

    @BeforeMethod
    public void setUp(){

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        HomePage page = new HomePage();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Прокликать слайдер")
    public void clickSlider() throws InterruptedException {

        HomePage page = new HomePage();

        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент Главной страницы не найден");
        }

        click(page.nextPage);
        sleep(500);
        click(page.prevPage);
        sleep(500);

    }

    @Test(description = "Переход по ссылке на лендинг Лайнер", priority = 1)
    public void clickLiner() {

        HomePage page = new HomePage();

        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент Главной страницы не найден");
        }

        click(page.liner);
        try {
            driver.findElement(page.landingLinerElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница лендинга не загрузилась");
        }

    }

    @Test(description = "Переход по ссылке на страницу поиска", priority = 2)
    public void clickSearchPage() {

        HomePage page = new HomePage();

        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент Главной страницы не найден");
        }

        click(page.toSearchPageLink);
        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница поиска не загрузилась");
        }

    }

    @Test(description = "Меню 'Все напрваления ДОМ.РФ'", priority = 3)
    public void menuAllDirectionsAhml() {

        HomePage page = new HomePage();

        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент Главной страницы не найден");
        }

        click(page.menuAllDirectionsAhml);
        try {
            driver.findElement(page.toSiteGroupLink);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Меню не развернулось");
        }

        click(page.toSiteGroupLink);
        try {
            driver.findElement(page.siteGroupPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент сайта группы не найден");
        }

        driver.navigate().back();
        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент Главной страницы не найден");
        }

        click(page.menuAllDirectionsAhml);
        try {
            driver.findElement(page.toSiteGroupLink);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Меню не развернулось");
        }

        click(page.menuAllDirectionsAhml);
        isElementNotPresent(page.toSiteGroupLink);

    }

    @Test(description = "Переход по ссылкам в хедере страницы", priority = 4)
    public void clickHeaderLinks() {

        HomePage page = new HomePage();
        LinerLandingPage linerpage = new LinerLandingPage();

        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент Главной страницы не найден");
        }

        click(page.findApartHeader);
        try {
            driver.findElement(page.searchPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница поиска не загрузилась");
        }

        click(page.logoHeader);
        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Главная страница не загрузилась");
        }

        click(page.rentersHeader);
        try {
            driver.findElement(linerpage.rentersPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница 'Арендаторам' не загрузилась");
        }
        driver.navigate().back();
        waitBy(page.homePageElement);

        click(page.partnersHeader);
        try {
            driver.findElement(linerpage.partnersPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница 'Партнерам' не загрузилась");
        }
        driver.navigate().back();
        waitBy(page.homePageElement);

        click(page.investorsHeader);
        try {
            driver.findElement(linerpage.investorsPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница 'Инвесторам' не загрузилась");
        }
        driver.navigate().back();
        waitBy(page.homePageElement);

        click(page.massMediaHeader);
        try {
            driver.findElement(linerpage.massMediaPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница 'СМИ о нас' не загрузилась");
        }
        driver.navigate().back();
        waitBy(page.homePageElement);

        click(page.cabinet);
        try {
            driver.findElement(linerpage.authPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница авторизации не загрузилась");
        }

        click(page.logoCabinet);
        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Главная страница не загрузилась");
        }

    }

    @Test(description = "Переход по ссылкам в футере страницы", priority = 5)
    public void clickFooterLinks() throws InterruptedException {

        HomePage page = new HomePage();

        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент Главной страницы не найден");
        }

        String handleBefore = driver.getWindowHandle();

        click(page.doc1);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc1 = "https://dev-bk.stage.domrf.orbita.center/documents/Lajner_tipovoi_dogovor_bronirovanija_s_fiz_licom.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc1));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.doc2);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc2 = "https://dev-bk.stage.domrf.orbita.center/documents/Lajner_dogovor_arendy.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc2));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.doc3);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc3 = "https://dev-bk.stage.domrf.orbita.center/documents/Lajner_soglasie_na_obrabotky_pers_dannih.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc3));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.facebook);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            driver.findElement(page.facebookElement);
            driver.getCurrentUrl().contains("https://www.facebook.com/domrf");
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы Facebook не найден");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.instagram);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            driver.findElement(page.instagramElement);
            driver.getCurrentUrl().contains("https://www.instagram.com/arenda_domrf");
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы Instagram не найден");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.vkontakte);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            driver.findElement(page.vkontakteElement);
            driver.getCurrentUrl().contains("https://vk.com/dom_rf1");
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы Вконтакте не найден");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

    }

    @Test(description = "Смена языка", priority = 6)
    public void changeLanguage() throws InterruptedException {

        HomePage page = new HomePage();

        try {
            driver.findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент Главной страницы не найден");
        }

        driver.findElement(page.changeToEnglish);

        click(page.changeToEnglish);
        try {
            driver.findElement(page.changeToRussian);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Перевод на английский язык не сработал");
        }

        sleep(500);

        click(page.changeToRussian);
        try {
            driver.findElement(page.changeToEnglish);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Перевод на русский язык не сработал");
        }

    }

}
