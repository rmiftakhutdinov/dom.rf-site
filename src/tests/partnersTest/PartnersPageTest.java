package tests.partnersTest;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.PartnersPage;
import java.util.concurrent.TimeUnit;

public class PartnersPageTest extends Helper {

    @BeforeMethod
    public void setUp(){

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        PartnersPage page = new PartnersPage();
        driver.get(page.pageURL);
        try {
            waitBy(page.partnersPageElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Элемент раздела 'Партнерам' не найден");
        }

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Открыть документ 'Порядок и условия финансирования арендных проектов'")
    public void openDocuments() throws InterruptedException {

        PartnersPage page = new PartnersPage();
        String handleBefore = driver.getWindowHandle();

        click(page.document);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc = "https://dev-bk.stage.domrf.orbita.center/documents/poryadok_i_usloviya_finansirovaniya.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc));
        driver.close();
        driver.switchTo().window(handleBefore);

    }

}
