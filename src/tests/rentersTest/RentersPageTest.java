package tests.rentersTest;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.RentersPage;
import java.util.concurrent.TimeUnit;

public class RentersPageTest extends Helper {

    @BeforeMethod
    public void setUp(){

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        RentersPage page = new RentersPage();
        driver.get(page.pageURL);
        try {
            waitBy(page.rentersPageElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Элемент раздела 'Арендаторам' не найден");
        }

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Свернуть и развернуть статьи ФАКа")
    public void hideAndShowFAQ() throws InterruptedException {

        RentersPage page = new RentersPage();

        click(page.parking);
        sleep(500);
        Assert.assertTrue(driver.findElement(page.parkingElement).getText().contains("Для жителей Арендного дома «Лайнер»"));
        click(page.parking);
        sleep(500);
        Assert.assertFalse(driver.findElement(page.parkingElement).isDisplayed());

        click(page.internet);
        sleep(500);

        Assert.assertTrue(driver.findElement(page.internetElement).getText().contains("Во все апартаменты заведен"));
        click(page.internet);
        sleep(500);
        Assert.assertFalse(driver.findElement(page.internetElement).isDisplayed());

    }

    @Test(description = "Открыть документы", priority = 1)
    public void openDocuments() throws InterruptedException {

        RentersPage page = new RentersPage();
        String handleBefore = driver.getWindowHandle();

        click(page.file1);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc1 = "https://dev-bk.stage.domrf.orbita.center/documents/Lajner_tipovoi_dogovor_bronirovanija_s_fiz_licom.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc1));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.file2);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc2 = "https://dev-bk.stage.domrf.orbita.center/documents/Lajner_soglasie_na_obrabotky_pers_dannih.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc2));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.file3);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc3 = "https://dev-bk.stage.domrf.orbita.center/documents/Lajner_dogovor_arendy.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc3));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.file4);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc4 = "https://dev-bk.stage.domrf.orbita.center/documents/Dolgosrochnyj_dogovor_arendy_s_FL_redakcija_1.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc4));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.file5);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc5 = "https://dev-bk.stage.domrf.orbita.center/documents/Lajner_dogovor_arendy_avto_fiz_lico.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc5));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.file6);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc6 = "https://dev-bk.stage.domrf.orbita.center/documents/Prejskurant_na_dopolnitelnye_uslugi_10_05.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc6));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.file7);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String doc7 = "https://dev-bk.stage.domrf.orbita.center/documents/living_rules.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(doc7));
        driver.close();
        driver.switchTo().window(handleBefore);

    }

}
